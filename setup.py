from setuptools import setup, find_packages

from os import path

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    readme = f.read()

setup(
    name='commonocean-io',
    version='2023.1',
    description='Python tool to read, write, and visualize CommonOcean scenarios and solutions for automated vessels.',
    keywords='autonomous automated vessels motion planning',
    author='Cyber-Physical Systems Group, Technical University of Munich',
    author_email='commonocean@lists.lrz.de',
    license="GNU General Public License v3.0",
    packages=find_packages(exclude=['doc', 'tutorials', 'documentation']),
    package_data={'commonocean': ['visualization/traffic_signs/*.png']},
    include_package_data=True,
    install_requires=[
        'commonroad-io==2023.1',
        'commonocean-vessel-models==1.0.0',
        'tqdm>=4.50.2',
        'networkx>=2.4',
        'ipywidgets~=7.5.1',
        'ipython-autotime~=0.1',
        'matplotlib~=3.5.0',
        'numpy~=1.22.4',
        'ipython~=7.23.1',
        'pyyaml~=5.3.1',
        'imageio~=2.9.0',
        'shapely>=1.6.4.post2',
        'setuptools>=42.0.1',
        'lxml>=4.2.2',
        'Pillow>=7.0.0',
        'iso3166>=1.0.1',
    ],
    extras_require={
        'doc': ['sphinx>=1.3.6',
                'graphviz>=0.3',
                'sphinx-autodoc-typehints>=1.3.0',
                'sphinx_rtd_theme>=0.4.1',
                'sphinx-gallery>=0.2.0',
                'ipython>=6.5.0'],
        'tests': ['lxml>=4.2.5',
                  'pytest>=3.8.0', ],
    },
    long_description_content_type='text/markdown',
    long_description=readme,
    classifiers=["Programming Language :: Python :: 3.8",
                   "Programming Language :: Python :: 3.9",
                   "Programming Language :: Python :: 3.10",
                   "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
                   "Operating System :: POSIX :: Linux",
                   "Operating System :: MacOS",
                   "Operating System :: Microsoft :: Windows"],
)
