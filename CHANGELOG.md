# Changelog

## [2023.1] - 2023-09-18

### Added

- New obstacle type "Waters Boundary" due to requirement in [CommonOcean DC](https://commonocean.cps.cit.tum.de/commonocean-dc)

### Fixed

- Creation of static obstacles for traffic signs such that no dublicate obstacles are created

### Changed

- Restructured State class so that it is now based on the commonroad-io state class and definition of State classes for different vessel dynamics.
- Removed duplicate classes and functions from commonroad-io to reduce maintance effort 
- The packages is no longer compatible with Python 3.7
- Updated documentation 

