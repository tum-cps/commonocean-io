Module Common
=============

File Reader
-----------

.. automodule:: commonocean.common.file_reader

``CommonOceanFileReader`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: CommonOceanFileReader
   :members:

File Writer
-----------

.. automodule:: commonocean.common.file_writer

``CommonOceanFileWriter`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: CommonOceanFileWriter
   :members:

``OverwriteExistingFile`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: OverwriteExistingFile
   :members:
   :undoc-members:
   :member-order: bysource

Solution
-----------

.. automodule:: commonocean.common.solution

``SolutionException`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: SolutionException
   :members:

``StateTypeException`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: StateTypeException
   :members:

``SolutionReaderException`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: SolutionReaderException
   :members:

``VesselType`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VesselType
   :members:

``VesselModel`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: VesselModel
   :members:

``CostFunction`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: CostFunction
   :members:

``StateFields`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: StateFields
   :members:

``XMLStateFields`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: XMLStateFields
   :members:

``StateType`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: StateType
   :members:

``TrajectoryType`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: TrajectoryType
   :members:

``SupportedCostFunctions`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: SupportedCostFunctions
   :members:

``PlanningProblemSolution`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: PlanningProblemSolution
   :members:

``Solution`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: Solution
   :members:

``CommonOceanSolutionReader`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: CommonOceanSolutionReader
   :members:

``CommonOceanSolutionWriter`` class
^^^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: CommonOceanSolutionWriter
   :members:
