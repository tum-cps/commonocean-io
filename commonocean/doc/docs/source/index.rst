CommonOcean Input-Output
=====

This tool includes the python package for representing benchmarks for marine motion planning. In addition,
some hand-crafted scenarios, an adapted version of the commonroad drivability checker, and student documentations are provided.
The structure of the repository is:

|   .
|   ├── documentation                    # Documentation of scenario specification
|   └──  commonocean                     # Source files
|        ├── common                      # Folders which represent the package structure
|        ├── ...                         
|        └── doc                         # ´Read the Docs´ documentation for the commonocean-io package

.. _documentation:

Documentation
------------

The full documentation of the API and introductory examples can be found under `commonocean.cps.cit.tum.de <https://commonocean.cps.cit.tum.de>`__.

For getting started, we recommend our `tutorials <https://commonocean.cps.cit.tum.de/commonocean-io>`__.

.. _requirements:

Requirements
------------

The required dependencies for running CommonOceanIO are:

* commonroad-io==2023.1
* tqdm>=4.50.2
* networkx>=2.4
* ipywidgets~=7.5.1
* ipython-autotime~=0.1
* matplotlib~=3.3.2
* numpy~=1.22.4
* ipython~=7.18.1
* pyyaml~=5.3.1
* imageio~=2.9.0
* shapely>=1.6.4.post2
* setuptools>=42.0.1
* lxml>=4.2.2
* Pillow>=7.0.0
* iso3166>=1.0.1
* pytest>=7.1.1
* commonocean-vessel-models==1.0.0

.. _installation:

Installation
------------

Create a new Anaconda environment for Python 3.7 (here called co37).
Run in your Terminal window:

.. code-block:: console

   $ conda create −n co38 python=3.8

Activate your environment

.. code-block:: console

   $ conda activate co38
   
Install the package by simply using pip and, if you want to use the jupyter notebook, also install jupyter

.. code-block:: console

   $ pip install commonocean-io
   $ conda install jupyter

Now everything is installed and you can run the main.py file or start jupyter notebook to run the example notebook

.. code-block:: console

   $ jupyter notebook

.. _contents:

Contents
------------

.. toctree::
   :maxdepth: 2

   api/index.rst

.. _contactinformation:

Contact information
-------------------

:Website: `https://commonocean.cps.cit.tum.de <https://commonocean.cps.cit.tum.de>`_
:Email: `commonocean@lists.lrz.de <commonocean@lists.lrz.de>`_
